function splitString(ipAddress) {
  //created a function
  if (typeof ipAddress === "string") {
    //checking whether our input is string or not
    let splitArray = ipAddress.split("."); //seperated string using split method
    if (splitArray.length === 4) {
      //checking whether ip address is ipv4 or not
      return splitArray;
    } else {
      return []; //if not returning empty string
    }
  } else {
    return "Input is not String";
  }
}
module.exports = splitString; //exporting function
