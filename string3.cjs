function findMonth(date) {
  //creating function
  if (typeof date === "string") {
    //checking whether of input is string or not
    let splitArray = date.split("/"); //splitting string with respect to "/"
    return splitArray;
  } else {
    return "Input is not a string";
  }
}
module.exports = findMonth; //exporting function
