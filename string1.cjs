function convertStringToNum(inputString) {
  //creating function
  if (typeof inputString === "string") {
    //checking input is string or not
    let newStrings = inputString.replaceAll("$", ""); //removing all $ by help of replaceAll mtd
    newStrings = newStrings.replaceAll(",", ""); //removing all , by help of replaceAll mtd
    if (isNaN(newStrings)) {
      //checking newString is number or not
      return 0;
    } else {
      return Number(newStrings); // return newString if it is number
    }
  } else {
    return "invalid string";
  }
}
module.exports = convertStringToNum; //exporting function
