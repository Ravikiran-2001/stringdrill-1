function findFullName(nameData) {
  //creating function
  if (typeof nameData === "object") {
    //checking our input is object or not
    let nameArray = Object.values(nameData); //retriving all data from object and stored it in array
    let fullName = nameArray.join(" "); //joined all data inside array and seperated it with space
    return fullName.toUpperCase(); //converted string to uppercase
  } else {
    return "input is not a object";
  }
}
module.exports = findFullName; //exporting function
