function convertToString(inputArray) {
  //creating function
  if (Array.isArray(inputArray)) {
    //checking input is array or not
    if (inputArray.length > 0) {
      //checking array is empty or not
      let convertedString = inputArray.join(" "); //joining all values using join method
      return convertedString + ".";
    } else {
      return ""; //if array is empty returning empty string
    }
  } else {
    return "invalid Array";
  }
}
module.exports = convertToString; //exporting function
